#include "Snowflake.h"
#define numMSPixels 4
#define numMemStar 6
#define numPixStarExt 6

#define numbranches 6
#define numISP      4      // How many pixels in one branch of the inside star
#define numOSP      6      // How many pixels in one branch of the inside star
#define numbar      6

int starPixels[numMSPixels] = {0,1,14,15};
int barPixels[numbar]       = {2, 3, 4, 5, 6, 7};

int InStarPixels [numISP] = {0, 1, 14, 15};
int OutStarPixels[numOSP] = {8, 9, 10, 11, 12, 13};

#define numInStarLEDs  numbranches * numISP
#define numOutStarLEDs numbranches * numOSP


/*****************************************************************************/

Snowflake::Snowflake(uint16_t n, uint8_t dpin, uint8_t cpin)
{
  strip = Adafruit_WS2801(n, dpin, cpin);
  numLEDs = n;
}

void Snowflake::begin(void) {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  strip.begin();
  // Update LED contents, to start they are all 'off'
  strip.show();
}
void Snowflake::setPixelColor(uint16_t n, uint32_t c){strip.setPixelColor(n, c);}
void Snowflake::Off(void) {
  int i;
  for (i=0; i<strip.numPixels(); i++){
      strip.setPixelColor(i, Color(0,0,0));
  }
  strip.show();
}
void Snowflake::Run(uint32_t c, int wait) {
  int i;
  for (i=0; i<strip.numPixels(); i++){
    strip.setPixelColor(i,c);
    strip.show();
    delay(wait);
  }
}
void Snowflake::Blink(uint32_t c, int wait){

}
//Theatre-style crawling lights.
void Snowflake::theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();
     
      delay(wait);
     
      for (int i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}
//Theatre-style crawling lights with rainbow effect
void Snowflake::theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
        for (int i=0; i < strip.numPixels(); i=i+3) {
          strip.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
        }
        strip.show();
       
        delay(wait);
       
        for (int i=0; i < strip.numPixels(); i=i+3) {
          strip.setPixelColor(i+q, 0);        //turn every third pixel off
        }
    }
  }
}
void Snowflake::ride(uint32_t c1, uint32_t c2, int wait, int width, int increment){
  uint32_t pixelColors[numLEDs];

  for (int i=0    ; i<width  ; i++){ pixelColors[i] = c2; }
  for (int i=width; i<numLEDs; i++){ pixelColors[i] = c1; }
  //
  for (int p=0; p<numLEDs; p++){
    for (int i=0; i<numLEDs; i++){ strip.setPixelColor(i, pixelColors[i]); }
    strip.show();

    for (int i=0; i<increment; i++){
      pixelColors[(width+p+i)%numLEDs] = c2;
      pixelColors[(p+i)%numLEDs]       = c1;
    }
    delay(wait);
  }
  // Tour complet
  for (int i=0; i<numLEDs; i++){strip.setPixelColor(i, pixelColors[i]);}
  strip.show();
}



// *** Vertical bar
void Snowflake::Bar_setPixelColor(uint16_t n, uint32_t c){
  strip.setPixelColor( 16*(n/numbar) +  barPixels[n%(numbar)] , c);
}
int Snowflake::getBarPixelNum(int ibranch, int i){return barPixels[i] + ibranch*numbranches;}

void Snowflake::BarOn(uint32_t c, int w1, int w2) {
  int i,p;
  for (i=0; i<6*6; i++){
    Bar_setPixelColor(i, c);
    if(w1>0){strip.show();delay(w1);}
  }
  if(w2>0){strip.show();delay(w2);}
}  

// *** Inside star
void Snowflake::InStar_setPixelColor(uint16_t n, uint32_t c){
  strip.setPixelColor( 16*(n/numISP) +  InStarPixels[n%(numISP)] , c);
}
void Snowflake::InStarOn(uint32_t c, int w1, int w2) {
  int i,p;
  for (i=0; i<numInStarLEDs; i++){
    InStar_setPixelColor(i, c);
    if(w1>0){strip.show();delay(w1);}
  }
  if(w2>0){strip.show();delay(w2);}
}

void Snowflake::InStarOff() {Snowflake::InStarOn(Color(0,0,0),0,1);}

void Snowflake::InStarRun(uint32_t c, int wait) {
  int i,p;
  for (p=0; p<numMemStar; p++){
    for (i=0; i<numMSPixels; i++){
       strip.setPixelColor(getPixelNum(p,i),c);
       strip.show();
       delay(wait);
    }
  }
}
void Snowflake::InStarRide(uint32_t c1, uint32_t c2, int wait, int width, int increment){
  uint32_t pixelColors[numInStarLEDs];

  for (int i=0    ; i<width  ; i++){ pixelColors[i] = c1; }
  for (int i=width; i<numInStarLEDs; i++){ pixelColors[i] = c2; }
  
  //
  for (int p=0; p<numInStarLEDs; p++){
    for (int i=0; i<numInStarLEDs; i++){ InStar_setPixelColor(i, pixelColors[i]); }
    strip.show();

    for (int i=0; i<increment; i++){
      pixelColors[(width+p+i)%numInStarLEDs] = c1;
      pixelColors[(p+i)%numInStarLEDs]       = c2;
    }
    delay(wait);
  }
  // Tour complet
  for (int i=0; i<numInStarLEDs; i++){strip.setPixelColor(i, pixelColors[i]);}
  strip.show();
}

void Snowflake::InStarBlink(uint32_t c, int wait) {
  int i,p;
  InStarOn(c,0,wait);
  InStarOff();
  delay(wait);
}

// *** Outside star
void Snowflake::OutStarOn(uint32_t c, int w1, int w2) {
  int i;
  for (i=0; i<numOutStarLEDs; i++){
    OutStar_setPixelColor(i, c);
    if(w1>0){strip.show();delay(w1);}
  }
  if(w2>0){strip.show();delay(w2);}
}


// *** Both stars
void Snowflake::starsOn(uint32_t c){Snowflake::starsRun(c, 1);}

void Snowflake::starsRun(uint32_t c, int wait) {
  int i;
  for (i=0;i<numbranches;i++){
    Snowflake::OutStar_setPixelColor( 6*i    , c);
    Snowflake::OutStar_setPixelColor( 6*i + 1, c);
    Snowflake::InStar_setPixelColor ( 4*i    , c);
    strip.show();
    delay(wait);

    Snowflake::OutStar_setPixelColor( 6*i + 2, c);
    Snowflake::InStar_setPixelColor ( 4*i + 1, c);
    strip.show();
    delay(wait);
      
    Snowflake::OutStar_setPixelColor( 6*i + 3, c);
    Snowflake::InStar_setPixelColor ( 4*i + 2, c);
    strip.show();
    delay(wait);
      
    Snowflake::OutStar_setPixelColor( 6*i + 4, c);
    Snowflake::OutStar_setPixelColor( 6*i + 5, c);
    Snowflake::InStar_setPixelColor ( 4*i + 3, c);
    strip.show();
    delay(wait);
  }
}

void Snowflake::starsRun2(uint32_t c1, uint32_t c2, int wait) {
  int i;
  i=0;

  
  Snowflake::OutStar_setPixelColor( 6*i    , c1);
  Snowflake::OutStar_setPixelColor( 6*i + 1, c1);
  Snowflake::InStar_setPixelColor ( 4*i    , c1);
  strip.show();
  delay(wait);

  Snowflake::OutStar_setPixelColor( 6*i + 2, c1);
  Snowflake::InStar_setPixelColor ( 4*i + 1, c1);
  strip.show();
  delay(wait);
      
  Snowflake::OutStar_setPixelColor( 6*i + 3, c1);
  Snowflake::InStar_setPixelColor ( 4*i + 2, c1);
  strip.show();
  delay(wait);
      
  Snowflake::OutStar_setPixelColor( 6*i + 4, c1);
  Snowflake::OutStar_setPixelColor( 6*i + 5, c1);
  Snowflake::InStar_setPixelColor ( 4*i + 3, c1);
  strip.show();
  delay(wait);

  for (i=1;i<numbranches;i++){
    Snowflake::OutStar_setPixelColor( 6*i        , c1);
    Snowflake::OutStar_setPixelColor( 6*i + 1    , c1);
    Snowflake::InStar_setPixelColor ( 4*i        , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1)    , c2);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 1, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1)    , c2);
    strip.show();
    delay(wait);

    Snowflake::OutStar_setPixelColor( 6*i + 2    , c1);
    Snowflake::InStar_setPixelColor ( 4*i + 1    , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 2, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1) + 1, c2);
    strip.show();
    delay(wait);
      
    Snowflake::OutStar_setPixelColor( 6*i + 3    , c1);
    Snowflake::InStar_setPixelColor ( 4*i + 2    , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 3, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1) + 2, c2);
    strip.show();
    delay(wait);
      
    Snowflake::OutStar_setPixelColor( 6*i + 4    , c1);
    Snowflake::OutStar_setPixelColor( 6*i + 5    , c1);
    Snowflake::InStar_setPixelColor ( 4*i + 3    , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 4, c2);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 5, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1) + 3, c2);
    strip.show();
    delay(wait);
  }
}


void Snowflake::starsRun3(uint32_t c1, uint32_t c2, int wait) {
  int i,j;
  i=0;
  
  Snowflake::OutStar_setPixelColor( 6*i    , c1);
  Snowflake::OutStar_setPixelColor( 6*i + 1, c1);
  Snowflake::InStar_setPixelColor ( 4*i    , c1);
  strip.show();
  delay(wait);

  Snowflake::OutStar_setPixelColor( 6*i + 2, c1);
  Snowflake::InStar_setPixelColor ( 4*i + 1, c1);
  strip.show();
  delay(wait);

  for (j=0;j<numbar;j++){ strip.setPixelColor(barPixels[j], c1); }
      
  Snowflake::OutStar_setPixelColor( 6*i + 3, c1);
  Snowflake::InStar_setPixelColor ( 4*i + 2, c1);
  strip.show();
  delay(wait);
      
  Snowflake::OutStar_setPixelColor( 6*i + 4, c1);
  Snowflake::OutStar_setPixelColor( 6*i + 5, c1);
  Snowflake::InStar_setPixelColor ( 4*i + 3, c1);
  strip.show();
  delay(wait);

  for (i=0;i<numbranches;i++){
    Snowflake::OutStar_setPixelColor( 6*i        , c1);
    Snowflake::OutStar_setPixelColor( 6*i + 1    , c1);
    Snowflake::InStar_setPixelColor ( 4*i        , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1)    , c2);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 1, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1)    , c2);
    strip.show();
    delay(wait);

    Snowflake::OutStar_setPixelColor( 6*i + 2    , c1);
    Snowflake::InStar_setPixelColor ( 4*i + 1    , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 2, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1) + 1, c2);
    strip.show();
    delay(wait);
      
    for (j=0;j<numbar;j++){ strip.setPixelColor(16*i + barPixels[j], c1); }
    for (j=0;j<numbar;j++){ strip.setPixelColor(16*(i-1) + barPixels[j], c2); }
    Snowflake::OutStar_setPixelColor( 6*i + 3    , c1);
    Snowflake::InStar_setPixelColor ( 4*i + 2    , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 3, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1) + 2, c2);
    strip.show();
    delay(wait);
      
    Snowflake::OutStar_setPixelColor( 6*i + 4    , c1);
    Snowflake::OutStar_setPixelColor( 6*i + 5    , c1);
    Snowflake::InStar_setPixelColor ( 4*i + 3    , c1);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 4, c2);
    Snowflake::OutStar_setPixelColor( 6*(i-1) + 5, c2);
    Snowflake::InStar_setPixelColor ( 4*(i-1) + 3, c2);
    strip.show();
    delay(wait);
  }
}

void Snowflake::branch(int n, uint32_t c, int w1, int w2){
  int count = numISP + numOSP + numbar;
  for (int i=0; i<count; i++){
    strip.setPixelColor(count*n + i, c);
    if (w1>0){delay(w1);strip.show();}
  }
  if (w2>0){delay(w2);strip.show();}
}
void Snowflake::branches(uint32_t bgcolor, uint32_t c, int w){
  for (int i=0;i<numbranches;i++){
    branch(i, c, 0, 0);
    for (int j=0  ; j<i          ; j++){ branch(j, bgcolor,0,0); }
    for (int j=i+1; j<numbranches; j++){ branch(j, bgcolor,0,0); }
    strip.show(); delay(w);
  }
}
//void starsBlink  (uint32_t c1, uint32_t c2, int wait){
//}

void Snowflake::Circle(uint32_t c, int wait){
  for (int i=0;i<6;i++){strip.setPixelColor(16*i,c);strip.setPixelColor(16*i+2,c);strip.setPixelColor(16*i+15,c);}
  strip.show();
  delay(wait);
}




// * Helper functions *
// Convert separate R,G,B into packed 32-bit RGB color.
// Packed format is always RGB, regardless of LED strand color order.
uint32_t Snowflake::Color(uint8_t r, uint8_t g, uint8_t b) {
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}


void Snowflake::OutStar_setPixelColor(uint16_t n, uint32_t c){
  strip.setPixelColor( 16*(n/numOSP) +  OutStarPixels[n%(numOSP)] , c);
}

int Snowflake::getPixelNum(int ibranch, int i){return i + ibranch*numbranches;}
int Snowflake::getInStarPixelNum (int ibranch, int i){return InStarPixels[i] + ibranch*numbranches;}
int Snowflake::getOutStarPixelNum(int ibranch, int i){return OutStarPixels[i] + ibranch*numbranches;}

uint16_t Snowflake::numPixels(void) {return strip.numPixels();}
uint16_t Snowflake::numBranches(void) {return numbranches;}
uint16_t Snowflake::InStar_numPixels(void) {return numInStarLEDs ;}
uint16_t Snowflake::OutStar_numPixels(void){return numOutStarLEDs;}
void Snowflake::show(void) {strip.show();}


//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Snowflake::Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Snowflake::Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Snowflake::Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Snowflake::Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
