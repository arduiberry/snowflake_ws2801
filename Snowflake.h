#ifndef __SNOWFLAKE__
#define __SNOWFLAKE__

#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
 #include <pins_arduino.h>
#endif

#include "Adafruit_WS2801.h"
//#include "SPI.h" // Comment out this line if using Trinket or Gemma
//#ifdef __AVR_ATtiny85__
// #include <avr/power.h>
//#endif

class Snowflake{
  
 public:

  Snowflake(uint16_t n, uint8_t dpin, uint8_t cpin);
  void
    begin(void),
    show(void),
    setPixelColor(uint16_t n, uint32_t c),
    
    Run (uint32_t c, int wait),
    Off(void),
    Blink(uint32_t c, int wait),
    theaterChase(uint32_t c, uint8_t wait),
    theaterChaseRainbow(uint8_t wait),
    ride(uint32_t c1, uint32_t c2, int wait, int width=5, int increment=1),
    branch(int n, uint32_t c, int wait1, int wait2),
    branches(uint32_t bgcolor, uint32_t c, int w),
    
    //Verticl bar
    Bar_setPixelColor(uint16_t n, uint32_t c),
    BarOn(uint32_t c, int w1, int w2),
    
    //Inside Star
    InStar_setPixelColor(uint16_t n, uint32_t c),
    InStarOn(uint32_t c, int w1, int w2),
    InStarBlink(uint32_t c, int wait),
    InStarRun (uint32_t c, int wait),
    InStarOff(),
    InStarRide(uint32_t c1, uint32_t c2, int wait, int width=5, int increment=1),

    //Outside Star
    OutStar_setPixelColor(uint16_t n, uint32_t c),
    OutStarOn(uint32_t c, int w1, int w2),
    OutStarBlink(uint32_t c, int wait),

    //Both Stars
    starsOn  (uint32_t c),
    starsRun  (uint32_t c, int wait),
    starsRun2   (uint32_t c1, uint32_t c2, int wait),
    starsRun3   (uint32_t c1, uint32_t c2, int wait),
    starsBlink  (uint32_t c1, uint32_t c2, int wait);
    //starsRun3  (uint32_t c1, uint32_t c2, int width, int wait, int boucle);

  void
    Circle(uint32_t c, int wait);
  
  uint16_t
    numPixels(void),
    numBranches(void),
    InStar_numPixels(void),
    OutStar_numPixels(void);
  
  static uint32_t
    Color(uint8_t r, uint8_t g, uint8_t b),
    Wheel(byte WheelPos);
  
 private:
  
  Adafruit_WS2801 strip;

  int
    getPixelNum(int ibranch, int i),
    getInStarPixelNum(int ibranch, int i),
    getOutStarPixelNum(int ibranch, int i),
    getBarPixelNum(int ibranch, int i),

    numLEDs;

  /* static const int nb_element_star = 5; */
  /* const int */
  /*   starPixels[nb_element_star] = {0,1,2,14,15}, // the number of first member of the star. */
  /*   numSpart = 6; // How many members do the star have. */

  /* const int */
  /*   numMemStar,    // Number of member to make the star */
  /*   numMSPixels  ; // Number of pixel in a member */
};
#endif // __SNOWFLAKE__


