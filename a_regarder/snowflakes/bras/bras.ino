// J'ai un bras de leds 13 avec le controler ...
//Attention ici le premier bras possede 14 leds

#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

// Choose which 2 pins you will use for output.
// Can be any valid output pins.
// The colors of the wires may be totally different so
// BE SURE TO CHECK YOUR PIXELS TO SEE WHICH WIRES TO USE!
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels
int nbledBras = 13;
int k;
int l,wait,blackwait;

// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Adafruit_WS2801 strip = Adafruit_WS2801(2*nbledBras + 1, dataPin, clockPin);

// Optional: leave off pin numbers to use hardware SPI
// (pinout is then specific to each board and can't be changed)
//Adafruit_WS2801 strip = Adafruit_WS2801(25);

// For 36mm LED pixels: these pixels internally represent color in a
// different format.  Either of the above constructors can accept an
// optional extra parameter: WS2801_RGB is 'conventional' RGB order
// WS2801_GRB is the GRB order required by the 36mm pixels.  Other
// than this parameter, your code does not need to do anything different;
// the library will handle the format change.  Examples:
//Adafruit_WS2801 strip = Adafruit_WS2801(25, dataPin, clockPin, WS2801_GRB);
//Adafruit_WS2801 strip = Adafruit_WS2801(25, WS2801_GRB);

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  strip.begin();
  // Update LED contents, to start they are all 'off'
  strip.show();
}

void loop() {
  for (k=0;k<5;k++)
  {
    fleches(0, Color(122,255,0), 5000);
    fleches(1, Color(122,255,0), 5000);
  }
  for (k=0;k<10;k++){  bas2haut(0, Wheel(map(k, 0, 10, 0, 255)), 400);  bas2haut(1, Wheel(map(k, 0, 10, 0, 255)), 400);}
  
  for (k=0;k<5;k++)
  {
    blink_bras(0, Wheel(map(k, 0, 5, 0, 255)), 200);
    blink_bras(1, Wheel(map(k, 0, 5, 0, 255)), 200);
  }

  //blink scheme 2
  for (k=0;k<6;k++)
  {
    ColorTopPart(0, Color(0,255,0), 500);
    ColorTopPart(0, Color(0,0,0), 0);
    ColorBotPart(0, Color(0,255,0), 500);
    ColorBotPart(0, Color(0,0,0), 0);

    ColorTopPart(1, Color(0,255,0), 500);
    ColorTopPart(1, Color(0,0,0), 0);
    ColorBotPart(1, Color(0,255,0), 500);
    ColorBotPart(1, Color(0,0,0), 0);
  }
  rainbow(0, 10);
  rainbow(1, 10);

    //blink scheme 2
  for (k=0;k<6;k++)
  {
    wait=1200;
    blackwait=150;
    ColorBotPart(0, Color(0,255,0), 0);
    ColorBotPart(1, Color(0,255,0), 0);
    for (l=0;l<3;l++){
      ColorTopPart(0, Color(0,255,0), wait/4-blackwait);
      ColorTopPart(1, Color(0,255,0), wait/4-blackwait);
      ColorTopPart(0, Color(0,0,0), blackwait);
      ColorTopPart(1, Color(0,0,0), blackwait);
    }
    ColorTopPart(0, Color(0,255,0), wait/4-blackwait);
    ColorTopPart(1, Color(0,255,0), wait/4-blackwait);
    ColorBotPart(0, Color(0,0,0), blackwait);
    ColorBotPart(1, Color(0,0,0), blackwait);
  }
  
  rainbowCycle(0, 20);
  rainbowCycle(1, 20);
  
}



// Slightly different, this one makes the rainbow wheel equally distributed 
// along the chain
void rainbowCycle(int bras, uint8_t wait) {
  int i, j;
  
  for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 25 colors in the wheel
    for (i=0; i < nbledBras; i++) {
      // tricky math! we use each pixel as a fraction of the full 96-color wheel
      // (thats the i / strip.numPixels() part)
      // Then add in j which makes the colors go around per pixel
      // the % 96 is to make the wheel cycle around
      strip.setPixelColor(pixelNumber(bras,  i), Wheel( ((i * 256 / nbledBras) + j) % 256) );
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}

//void rainbow_fixe(int bras, int wait) {
//  int j;
//  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
//      ColorPartII(0, bras, Wheel( j % 255), wait);
//      ColorPartII(1, bras, Wheel( j % 255), wait);
//  }
//}

void rainbow(int bras, int wait)
{ int j;
  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
      ColorPartWheel(bras, 0, j, wait);
      ColorPartWheel(bras, 1, j, wait);
  }
}
//void rainbowFCT(int bras, int wait, void (*FCT)(int bras, int j, int wait))
//{ int j;
//  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
//      FCT(bras, j, wait);
//  }
//}
//
//void ColorWheel(int bras, int j, int wait){ColorPartWheel(&bras, 0, &j, &wait);  ColorPartWheel(&bras, 1, &j, &wait);}
//void rainbow_fixe(int bras, int j, int wait) {ColorPartII(bras, 0, Wheel( j % 255), wait);ColorPartII(bras, 1, Wheel( j % 255), wait);}

void bas2haut(int bras, uint32_t color, uint8_t wait)
{
  int i;
  
  strip.setPixelColor(pixelNumber(bras,  0), color);
  strip.show(); 
  delay(wait);
  strip.setPixelColor(pixelNumber(bras, 1), color);
  strip.show(); 
  delay(wait);

  for (i=0;i<2;i++)
  {
  strip.setPixelColor(pixelNumber(bras,  2) + i, color);
  strip.setPixelColor(pixelNumber(bras, 12) - i, color);
  strip.show(); 
  delay(wait);     
  }

  strip.setPixelColor(pixelNumber(bras, 4), color);
  strip.show(); 
  delay(wait);
  strip.setPixelColor(pixelNumber(bras, 5), color);
  strip.show(); 
  delay(wait);

  for (i=0;i<2;i++)
  {
  strip.setPixelColor(pixelNumber(bras, 6) + i, color);
  strip.setPixelColor(pixelNumber(bras,10) - i, color);
  strip.show(); 
  delay(wait);     
  }

  strip.setPixelColor(pixelNumber(bras,8), color);
  strip.show();   
}

void fleches(int bras, uint32_t color, uint8_t wait)
{
 
  strip.setPixelColor(pixelNumber(bras, 0), color);
  strip.setPixelColor(pixelNumber(bras, 1), color);
  strip.setPixelColor(pixelNumber(bras, 2), color);
  strip.setPixelColor(pixelNumber(bras, 4), color);
  strip.setPixelColor(pixelNumber(bras,12), color);
  strip.show();
  delay(wait);
  black_bras(bras,wait/2);

  strip.setPixelColor(pixelNumber(bras, 4), color);
  strip.setPixelColor(pixelNumber(bras, 5), color);
  strip.setPixelColor(pixelNumber(bras, 6), color);
  strip.setPixelColor(pixelNumber(bras, 8), color);
  strip.setPixelColor(pixelNumber(bras,10), color);
  strip.show();
  delay(wait);
  black_bras(bras,wait/2);
}
void blink_bras(int bras, uint32_t color, uint8_t wait)
{
  int i;
  for (i=0;i<nbledBras;i++)
  {
    strip.setPixelColor(pixelNumber(bras,i), color);
  }
  strip.show();
  delay(wait);

  black_bras( bras, wait);
}

void black_bras(int bras, uint8_t wait)
{
  int i;
  for (i=0;i<nbledBras;i++)
  {
    strip.setPixelColor(pixelNumber(bras,i), Color(0,0,0));
  }
  strip.show();
  delay(wait);
}

void ColorPartWheel(int bras, int where, int j, int wait)
{
  //segment vertical
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), Wheel( ( pixelNumber(bras, 4*where + 0) + j) % 255));
  strip.show();
  delay(wait);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), Wheel( ( pixelNumber(bras, 4*where + 1) + j) % 255));
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), Wheel( ( pixelNumber(bras, 4*where + 2)+ j) % 255));
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), Wheel( ( pixelNumber(bras,12 - 2*where) + j) % 255));
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 3), Wheel( ( pixelNumber(bras, 4*where + 3) + j) % 255));
  strip.setPixelColor(pixelNumber(bras,11 - 2*where), Wheel( ( pixelNumber(bras,11 - 2*where) + j) % 255));
  strip.show();
  delay(wait);
}

void ColorPartII(int bras, int where, uint32_t color, int wait)
{
  //segment vertical
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), color);
  strip.show();
  delay(wait);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), color);
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), color);
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), color);
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 3), color);
  strip.setPixelColor(pixelNumber(bras,11 - 2*where), color);
  strip.show();
  delay(wait);
}

void ColorTopPart(int bras, uint32_t color, int wait){ColorPart(1, bras, color, wait);}
void ColorBotPart(int bras, uint32_t color, int wait){ColorPart(0, bras, color, wait);}

void ColorPart(int where, int bras, uint32_t color, int wait)
{
  //segment vertical
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), color);
  //segment oblique gauche
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 3), color);
  //segment oblique droit
  strip.setPixelColor(pixelNumber(bras,11 - 2*where), color);
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), color);
  strip.show();
  delay(wait);
}
/* Helper functions */

// "i" est la position  (commence à 0) de la leds dans bras comme si celui-ci n'était pas chainé à d'autres.
// "bras" est le numéro du bras (commence à 0) dans la chaine
// renvoit le numéro de la led dans la chaine.
int pixelNumber(int bras, int i){ return nbledBras*bras + 1 + i;}

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
