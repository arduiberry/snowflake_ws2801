// J'ai un bras de leds 13 avec le controler ...
//Attention ici le premier bras possede 14 leds

#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

// Choose which 2 pins you will use for output.
// Can be any valid output pins.
// The colors of the wires may be totally different so
// BE SURE TO CHECK YOUR PIXELS TO SEE WHICH WIRES TO USE!
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels
int nbledBras = 13;
int nbBras    = 2;
int ibras;
int k;
int l,wait,blackwait;
uint32_t black;
// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Adafruit_WS2801 strip = Adafruit_WS2801(nbBras*nbledBras + 1, dataPin, clockPin);

// Optional: leave off pin numbers to use hardware SPI
// (pinout is then specific to each board and can't be changed)
//Adafruit_WS2801 strip = Adafruit_WS2801(25);

// For 36mm LED pixels: these pixels internally represent color in a
// different format.  Either of the above constructors can accept an
// optional extra parameter: WS2801_RGB is 'conventional' RGB order
// WS2801_GRB is the GRB order required by the 36mm pixels.  Other
// than this parameter, your code does not need to do anything different;
// the library will handle the format change.  Examples:
//Adafruit_WS2801 strip = Adafruit_WS2801(25, dataPin, clockPin, WS2801_GRB);
//Adafruit_WS2801 strip = Adafruit_WS2801(25, WS2801_GRB);

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  black = Color(0,0,0);
  strip.begin();
  // Update LED contents, to start they are all 'off'
  strip.show();
}

void loop() {
  
  one(Wheel(100), Wheel(1200), 200,5);
  for (k=0;k<10;k++) {progress_bas2haut(Wheel(map(4*k, 0, 40, 20, 255)), 100);}
//  all_haut2bas(Wheel(0), 200);
//  all_haut2bas(Wheel(100), 200);
//  all_haut2bas(Wheel(1200), 200);

  //rainbowCycle(20);
  for (k=0;k<5;k++)  {  all_fleches(Wheel(map(2*k, 0, 10, 0, 255)), 200, 2);}
  for (k=0;k<10;k++) {  all_bas2haut(Wheel(map(k, 0, 10, 0, 255)), 200);}
  for (k=0;k<5;k++)  {  all_flechesII(Wheel(map(3*k, 0, 15, 80, 255)), 200, 2);}
  for (k=0;k<5;k++)  {  blink_top_then_bot( Wheel(map(4*k, 0, 20, 20, 255)), 200, 2);}
}

// ************************************
// fonction sur toutes les branches
// ************************************
void blink_top_then_bot(uint32_t color, int wait, int boucle) {
  int k, ibras;

  //blink scheme 2
  for (k=0;k<boucle;k++)
  {
    // On allume les n-1 bras sans delay
    for (ibras=0;ibras<nbBras;ibras++)
    {
      ColorTopPart(ibras, color,   0);
    }
    // Allumage du dernier bras avec delay
    ColorTopPart(nbBras-1, color, wait);
    //Extension des n bras
    for (ibras=0;ibras<nbBras;ibras++)
    {
      ColorTopPart(ibras, Color(0,0,0), 0);
    }

    // et on recommence pour la partie basse
    for (ibras=0;ibras<nbBras-1;ibras++)
    {
      ColorBotPart(ibras, color,   0);
    }
    ColorBotPart(nbBras-1, color, wait);
    for (ibras=0;ibras<nbBras;ibras++)
    {
      ColorBotPart(ibras, Color(0,0,0), 0);
    }
  }
}

void all_fleches(uint32_t color, int wait, int boucle) {
  int k, ibras, where;
  strip.setPixelColor(  0, color);

  for  (k=0;k<boucle;k++)
  { 
    // ** 
    for (where=0;where<2;where++)
    {
      for (ibras=0;ibras<nbBras;ibras++) { Arrow(ibras, where, color, 0); }
      Arrow(nbBras-1, where, color, wait);
      
      for (ibras=0;ibras<nbBras;ibras++){ black_bras( ibras, 0); }
      black_bras( nbBras-1, 0);
    } 
  }
}
void all_flechesII(uint32_t color, int wait, int boucle) {
  int k, ibras, where;
  strip.setPixelColor(  0, color);
  for  (k=0;k<boucle;k++)
  { 
    // ** 
      for (ibras=0;ibras<nbBras;ibras++) { Arrow(ibras, (ibras+k)%2, color, 0); }
      Arrow(nbBras-1, (nbBras-1+k)%2, color, wait);
      
      for (ibras=0;ibras<nbBras;ibras++){ black_bras( ibras, 0); }
      black_bras( nbBras-1, 0);
  }
}
void progress_bas2haut(uint32_t color, int wait)
{
  int i; 
  
    strip.setPixelColor( 0, color);
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  0), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  0), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  1), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  1), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  2), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 12), color);}
    strip.show(); delay(wait);
    
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  2), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 12), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  3), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 11), color);}
    strip.show(); delay(wait);


    
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  3), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 11), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  4), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  4), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  5), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  5), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  6), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 10), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  6), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 10), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  7), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  9), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  7), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  9), black);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  8), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  8), black);}
    strip.show(); delay(wait);
}
void all_bas2haut(uint32_t color, int wait)
{
  int i; 
  
    strip.setPixelColor( 0, color);
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  0), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  1), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  2), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 12), color);}
    strip.show(); delay(wait);
    
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  3), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 11), color);}
    strip.show(); delay(wait);


    
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  4), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  5), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  6), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 10), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  7), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  9), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  8), color);}
    strip.show(); delay(wait);
}
void all_haut2bas(uint32_t color, int wait)
{
  int i, pixel;

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  8), color);}
    strip.show(); delay(wait);


    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  7), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  9), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  6), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 10), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  5), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  4), color);}
    strip.show(); delay(wait);


    
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  3), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 11), color);}
    strip.show(); delay(wait);

    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  2), color);}
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras, 12), color);}
    strip.show(); delay(wait);
    
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  1), color);}
    strip.show(); delay(wait);
    for (ibras=0;ibras<nbBras;ibras++) { strip.setPixelColor(pixelNumber(ibras,  0), color);}
    strip.show(); delay(wait);

    strip.setPixelColor( 0, color);
    strip.show(); delay(wait);
}

//*************************************
// EFFETS
//*************************************
void one(uint32_t color1, uint32_t color2, int wait, int boucle){
  int i, b;
  for (i=0;i<boucle;i++){
  all_haut2bas(color1, wait);

  for (ibras=0;ibras<nbBras;ibras++) { 
    strip.setPixelColor(pixelNumber(ibras,  8), color2);
    strip.setPixelColor(0, black);
  }
    strip.show(); delay(wait);
    
  for (ibras=0;ibras<nbBras;ibras++) { 
    ligne_quatre(ibras, color2);
    strip.setPixelColor(pixelNumber(ibras,  0), black);
    strip.setPixelColor(pixelNumber(ibras,  1), black);
  }
    strip.show(); delay(wait);
  for (ibras=0;ibras<nbBras;ibras++) { 
    ligne_trois(ibras, color2);
    ligne_un(ibras, black);
  }
    strip.show(); delay(wait);
  for (ibras=0;ibras<nbBras;ibras++) { 
    strip.setPixelColor(pixelNumber(ibras,  4), color2);
    strip.setPixelColor(pixelNumber(ibras,  5), color2);
    ligne_deux(ibras, black);
  }
    strip.show(); delay(wait);
  //blink
  for (b=0;b<5;b++){
    for (ibras=0;ibras<nbBras;ibras++) {
      strip.setPixelColor(pixelNumber(ibras,  8), color2);
      ligne_quatre(ibras, color2);
      ligne_trois(ibras, color2);
      strip.setPixelColor(pixelNumber(ibras,  4), black);
      strip.setPixelColor(pixelNumber(ibras,  5), black);
    }
    strip.show(); delay(wait/2);

    for (ibras=0;ibras<nbBras;ibras++) {
      strip.setPixelColor(pixelNumber(ibras,  8), black);
      ligne_quatre(ibras, black);
      ligne_trois(ibras, black);
      strip.setPixelColor(pixelNumber(ibras,  4), color2);
      strip.setPixelColor(pixelNumber(ibras,  5), color2);
    }
    strip.show(); delay(wait/2);
  }
    for (ibras=0;ibras<nbBras;ibras++) {  black_bras(ibras, 10);}
  }
}
// ************************************
// fonction sur un bras
// ************************************

void Arrow(int bras, int where, uint32_t color, int wait)
{
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 4), color);
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), color);
  strip.show();
  delay(wait);
}

// Slightly different, this one makes the rainbow wheel equally distributed 
// along the chain
void rainbowCycle( int wait) {
  int i, j;
  
  for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 25 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      // tricky math! we use each pixel as a fraction of the full 96-color wheel
      // (thats the i / strip.numPixels() part)
      // Then add in j which makes the colors go around per pixel
      // the % 96 is to make the wheel cycle around
      strip.setPixelColor(i, Wheel( ((i * 256 / nbledBras) + j) % 256) );
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}

//void rainbow_fixe(int bras, int wait) {
//  int j;
//  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
//      ColorPartII(0, bras, Wheel( j % 255), wait);
//      ColorPartII(1, bras, Wheel( j % 255), wait);
//  }
//}

void rainbow(int bras, int wait)
{ int j;
  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
      ColorPartWheel(bras, 0, j, wait);
      ColorPartWheel(bras, 1, j, wait);
  }
}
//void rainbowFCT(int bras, int wait, void (*FCT)(int bras, int j, int wait))
//{ int j;
//  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
//      FCT(bras, j, wait);
//  }
//}
//
//void ColorWheel(int bras, int j, int wait){ColorPartWheel(&bras, 0, &j, &wait);  ColorPartWheel(&bras, 1, &j, &wait);}
//void rainbow_fixe(int bras, int j, int wait) {ColorPartII(bras, 0, Wheel( j % 255), wait);ColorPartII(bras, 1, Wheel( j % 255), wait);}

void blink_bras(int bras, uint32_t color, uint8_t wait)
{
  int i;
  for (i=0;i<nbledBras;i++)
  {
    strip.setPixelColor(pixelNumber(bras,i), color);
  }
  strip.show();
  delay(wait);

  black_bras( bras, wait);
}

void black_bras(int bras, int wait)
{
  int i;
  for (i=0;i<nbledBras;i++)
  {
    strip.setPixelColor(pixelNumber(bras,i), Color(0,0,0));
  }
  strip.show();
  delay(wait);
}

void ColorPartWheel(int bras, int where, int j, int wait)
{
  //segment vertical
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), Wheel( ( pixelNumber(bras, 4*where + 0) + j) % 255));
  strip.show();
  delay(wait);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), Wheel( ( pixelNumber(bras, 4*where + 1) + j) % 255));
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), Wheel( ( pixelNumber(bras, 4*where + 2)+ j) % 255));
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), Wheel( ( pixelNumber(bras,12 - 2*where) + j) % 255));
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 3), Wheel( ( pixelNumber(bras, 4*where + 3) + j) % 255));
  strip.setPixelColor(pixelNumber(bras,11 - 2*where), Wheel( ( pixelNumber(bras,11 - 2*where) + j) % 255));
  strip.show();
  delay(wait);
}

void ColorPartII(int bras, int where, uint32_t color, int wait)
{
  //segment vertical
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), color);
  strip.show();
  delay(wait);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), color);
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), color);
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), color);
  strip.show();
  delay(wait);

  //
  strip.setPixelColor(pixelNumber(bras, 4*where + 3), color);
  strip.setPixelColor(pixelNumber(bras,11 - 2*where), color);
  strip.show();
  delay(wait);
}

void ColorTopPart(int bras, uint32_t color, int wait){ColorPart(1, bras, color, wait);}
void ColorBotPart(int bras, uint32_t color, int wait){ColorPart(0, bras, color, wait);}

void ColorPart(int where, int bras, uint32_t color, int wait)
{
  //segment vertical
  strip.setPixelColor(pixelNumber(bras, 4*where + 0), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 1), color);
  //segment oblique gauche
  strip.setPixelColor(pixelNumber(bras, 4*where + 2), color);
  strip.setPixelColor(pixelNumber(bras, 4*where + 3), color);
  //segment oblique droit
  strip.setPixelColor(pixelNumber(bras,11 - 2*where), color);
  strip.setPixelColor(pixelNumber(bras,12 - 2*where), color);
  strip.show();
  delay(wait);
}
/* Helper functions */
void ligne_un(int ibras, uint32_t color){
  strip.setPixelColor(pixelNumber(ibras,  2), color);
  strip.setPixelColor(pixelNumber(ibras, 12), color);
}
void ligne_deux(int ibras, uint32_t color){
  strip.setPixelColor(pixelNumber(ibras,  3), color);
  strip.setPixelColor(pixelNumber(ibras, 11), color);
}
void ligne_trois(int ibras, uint32_t color){
  strip.setPixelColor(pixelNumber(ibras,  6), color);
  strip.setPixelColor(pixelNumber(ibras, 10), color);
}
void ligne_quatre(int ibras, uint32_t color){
  strip.setPixelColor(pixelNumber(ibras,  7), color);
  strip.setPixelColor(pixelNumber(ibras,  9), color);
}

// "i" est la position  (commence à 0) de la leds dans bras comme si celui-ci n'était pas chainé à d'autres.
// "bras" est le numéro du bras (commence à 0) dans la chaine
// renvoit le numéro de la led dans la chaine.
int pixelNumber(int bras, int i){ return nbledBras*bras + 1 + i;}

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
