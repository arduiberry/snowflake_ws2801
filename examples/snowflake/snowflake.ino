#include "Snowflake.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

// Choose which 2 pins you will use for output.
// Can be any valid output pins.
// The colors of the wires may be totally different so
// BE SURE TO CHECK YOUR PIXELS TO SEE WHICH WIRES TO USE!
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels
int nbledBras    = 16;
int i,k;
uint32_t color;
// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Snowflake flocon = Snowflake(6*nbledBras, dataPin, clockPin);
void setup() {
  // put your setup code here, to run once:
  flocon.begin();

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
}

void loop() {
  //* effet 1 from Adafruit librairy
  flocon.theaterChaseRainbow(50);

  //* effet 2
  for (i=0;i<10;i++){ 
    flocon.branches(flocon.Wheel(map(10*i,0,100,0,254)),flocon.Wheel(map(10*(i+1),0,100,0,254)),400);
    flocon.branch(5,flocon.Wheel(map(10*i+1,0,100,0,254)),0,0);
    flocon.branch(0,flocon.Wheel(map(10*(i+1),0,100,0,254)),0,1);
    for (int j=1;j<5;j++){flocon.branch(j,flocon.Wheel(map(10*(i+1),0,100,0,254)),0,400);}
    } 

  //* effet 3

// ============== debut effet
for (int i=0; i<5; i++){
  color = flocon.Wheel(random(254));
  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+7,color);
  }
  flocon.show();delay(400);
  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+6,color);  
    flocon.setPixelColor(16*br+8,color);
    flocon.setPixelColor(16*br+13,color);
  }
  flocon.show();delay(400);

  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+5,color);
    flocon.setPixelColor(16*br+9,color);
    flocon.setPixelColor(16*br+12,color);
  }
  flocon.show();delay(400);

  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+4,color);
    flocon.setPixelColor(16*br+10,color);
    flocon.setPixelColor(16*br+11,color);
  }
  flocon.show();delay(400);
  
  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+3,color);
  }
  flocon.show();delay(400);
  
  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+2,color);
    flocon.setPixelColor(16*br+0,color);
    flocon.setPixelColor(16*br+15,color);
  }
  flocon.show();delay(400);
  
  for (int br=0;br<6;br++){
    flocon.setPixelColor(16*br+1,color);
    flocon.setPixelColor(16*br+14,color);
  }
  flocon.show();delay(400);

for (int i=0;i<5;i++){
  color = flocon.Wheel(random(254));
  flocon.Off();
  delay(400);
  flocon.OutStarOn(color,0,400);
  flocon.Circle(color,400);
  flocon.Off();
  delay(400);
  flocon.OutStarOn(color,0,400);
  flocon.InStarOn(color,0,400);
}
}
// ============== fin effet

  for (i=0;i<5;i++){
    flocon.Run(flocon.Wheel(map(10*i,0,50,0,254)),10);
  }


// ============== Blink
  flocon.Off();
  for (k=0;k<20;k++){
    flocon.OutStarOn(flocon.Color(125,10,10),0,0);
    flocon.InStarOn(0,0,0);
    flocon.BarOn(0,0,200);
    flocon.InStarOn(flocon.Color(125,210,210),0,0);
    flocon.OutStarOn(flocon.Color(0,0,0),0,0);
    flocon.BarOn(flocon.Color(125,210,210),0,200);
  }
  
  flocon.Off();
  for (k=0;k<5;k++){ flocon.starsRun(flocon.Wheel(map(20*k,0,100,0,254)), 30);}
  
  flocon.Off();
  for (i=0;i<10;i++){ flocon.starsRun2(flocon.Color(0,120,10), flocon.Color(125,0,0), 30);}

  flocon.Off();
  for (i=0;i<10;i++){ flocon.starsRun3(flocon.Color(0,120,10), flocon.Color(125,0,0), 50);}
  
  
}
